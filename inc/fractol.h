/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lmunoz-q <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 22:20:19 by lmunoz-q          #+#    #+#             */
/*   Updated: 2018/04/10 21:35:44 by lmunoz-q         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H

# define SIZEX 1300
# define SIZEY 1300


typedef struct		s_cplx
{
	float			r;
	float			i;
}					t_cplx;

typedef struct		s_v
{
	float			ulx;
	float			uly;
	float			drx;
	float			dry;
	float			ptx;
	float			pty;
	float			r;
	float			i;
}					t_v;

typedef struct		s_m
{
	void			*mlx;
	void			*win;
	void			*img;
	int				bbp;
	int				sizeline;
	int				endn;
	int				size;
}					t_m;

typedef struct		s_e
{
	int				imax;
	int				i;
	int				x;
	int				y;
	float			click;
	float			zoom;
	float			x1;
	float			x2;
	float			y1;
	float			y2;
	float			taillex;
	float			tailley;
	int				*timg;
	float			imgx;
	float			imgy;
	int				color_bluecy;
	int				color_bluesty;
	t_m				*m;
}					t_e;

void		mandelbrot(t_e *env);
void		fractale(t_e *env, char *s);
t_e			*init(t_e *env, char *s);
t_cplx		gratio(t_cplx up_left, t_cplx down_right, t_cplx point);
t_cplx		appratio(t_cplx up_left, t_cplx down_right, t_cplx point);
t_v			get_rat(t_v v);
t_v			app_rat(t_e e, t_v v);
void		refresh(t_e *env);
int			mouse_hook(int button, int x, int y, t_e *env);
int			key_hook(int keycode, t_e *env);
int			name(char *s);

#endif
