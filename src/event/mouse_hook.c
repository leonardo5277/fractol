/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mouse_hook.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lmunoz-q <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/03 23:08:50 by lmunoz-q          #+#    #+#             */
/*   Updated: 2018/04/10 23:10:06 by lmunoz-q         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/fractol.h"
#include "../../libft/libft.h"

static void		vecter(t_v *v, int x, int y)
{
	v->ulx = 0;
	v->uly = 0;
	v->drx = SIZEX;
	v->dry = SIZEY;
	v->ptx = x;
	v->pty = y;
}

int	mouse_hook(int button, int x, int y, t_e *e)
{
	t_v a;
	t_v b;
//	t_cplx a;
//	t_cplx b;
	float m;

	ft_putnbr(button);
	ft_putstr("\n");
	if (button == 1 || button == 2 || button == 4 || button == 5)
	{
		vecter(&a, x, y);
		a = get_rat(a);
//		a = gratio((t_cplx){0, 0}, (t_cplx){SIZEX, SIZEY}, (t_cplx){x, y});
		a = app_rat(*e, a);
//		a = appratio((t_cplx){e->x1, e->y1}, (t_cplx){e->x2, e->y2}, a); // on choppe le complex click;
		m = e->zoom;
		if (button == 2 || button == 5)
			m = 1 / m;                                    // on reduit a 90% ou on augmente a 110%
		e->x1 *= m;
		e->x2 *= m;
		e->y1 *= m;
		e->y2 *= m;
		b = get_rat(a);
//		b = gratio((t_cplx){0, 0}, (t_cplx){SIZEX, SIZEY}, (t_cplx){x, y});
//		b = appratio((t_cplx){e->x1, e->y1}, (t_cplx){e->x2, e->y2}, b);         // on choppe le complex click apres multiplucation
		b = app_rat(*e, a);
		a.r = a.r - b.r;
		a.i = a.i - b.i;                       // on recupere le vecteur BA qui permet de repositioner B sur A
		e->x1 += a.r;
		e->x2 += a.r;
		e->y1 += a.i;
		e->y2 += a.i;      // on applique le vecteur BA aux coordonnes (qui au depart etaien -2.2-2.2)
		refresh(e);
	}
	return(0);
}
