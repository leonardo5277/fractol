/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_hook.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lmunoz-q <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/03 23:43:00 by lmunoz-q          #+#    #+#             */
/*   Updated: 2018/04/10 11:34:15 by lmunoz-q         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/fractol.h"
#include <stdio.h>
#include <stdlib.h>

int	key_hook(int keycode, t_e *e)
{
	printf("keycode : %d\n", keycode);

	float tmp;
	if (keycode == 53 || keycode == 12)
		exit(0);
	if (keycode == 69 || keycode == 78 || keycode == 6 || keycode == 7)
	{
		if (keycode == 6)
			e->imax = 2000;
		else if (keycode == 7)
			e->imax = 50;
		else
			e->imax += keycode == 69 ? 1 : -1;
		refresh(e);
	}
	else if (keycode >= 123 && keycode <= 126)
	{
		if (keycode == 124)
		{
			tmp = (e->x2 - e->x1) / 10;
			e->x1 += tmp;
			e->x2 += tmp;
		}
		else if (keycode == 125)
		{
			tmp = (e->y2 - e->y1) / 10;
			e->y1 += tmp;
			e->y2 += tmp;
		}
		else if (keycode == 126)
		{
			tmp = (e->y2 - e->y1) / 10;
			e->y1 -= tmp;
			e->y2 -= tmp;
		}
		else
		{
			tmp = (e->x2 - e->x1) / 10;
			e->x1 -= tmp;
			e->x2 -= tmp;
		}
		refresh(e);
	}
	if (keycode == 35 || keycode == 46)
	{
		e->zoom += keycode == 46 ? 0.1 : -0.1;
		refresh(e);
	}
	return (0);
}
