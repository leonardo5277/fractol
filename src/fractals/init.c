/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lmunoz-q <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/27 23:56:20 by lmunoz-q          #+#    #+#             */
/*   Updated: 2018/04/10 16:37:38 by lmunoz-q         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minilibx_macos/mlx.h"
#include "../../inc/fractol.h"
#include "../../libft/libft.h"
#include <stdio.h>

t_e	*init(t_e *e, char *s)
{
	s = "lol";
	e->imax = 30;
	e->x1 = -2;
	e->x2 = 2;
	e->y1 = -2;
	e->y2 = 2;
	e->taillex = SIZEX;
	e->tailley = SIZEY;
	e->imgx = (e->x2 - e->x1);
	e->imgy= (e->y2 - e->y1);
	e->zoom = 0.8;
	e->color_bluesty = (e->imax + 100) * 3 + (e->imax * 4 << 8) + (e->imax * 5 << 16);
	e->color_bluecy = (e->i * 150) + (e->i * 150 << 8) + (e->i * 150 >> 16);
//	e->color_bluecy = ((e->i * 150) % 255) + ((e->i * 150) % 255 << 8) + ((e->i * 150 ) % 255 >> 16);
	return (e);
}
