/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractale.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lmunoz-q <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/27 23:02:32 by lmunoz-q          #+#    #+#             */
/*   Updated: 2018/04/04 23:15:54 by lmunoz-q         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minilibx_macos/mlx.h"
#include "../../inc/fractol.h"
#include "../../libft/libft.h"
#include <stdio.h>

void	fractale(t_e *e, char *s)
{
	if (ft_strcmp(s, "mandelbrot") == 0)
		mandelbrot(init(e, s));
}
