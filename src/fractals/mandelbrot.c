/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mandelbrot.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lmunoz-q <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/26 12:29:08 by lmunoz-q          #+#    #+#             */
/*   Updated: 2018/04/10 23:26:19 by lmunoz-q         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minilibx_macos/mlx.h"
#include "../../libft/libft.h"
#include "../../inc/fractol.h"
#include <stdio.h>

static void		vecter(t_v *v, t_e *e)
{
	v->ulx = 0;
	v->uly = 0;
	v->drx = SIZEX;
	v->dry = SIZEY;
	v->ptx = e->x;
	v->pty = e->y;
}

void			mandelbrot(t_e *e)
{
	t_v v;

	while (e->x < SIZEX)
	{
		while (e->y < SIZEY)
		{
			vecter(&v, e);
//			t_cplx r;
//			t_cplx c;
			t_cplx z;

//			r = gratio((t_cplx){0, 0}, (t_cplx){SIZEX, SIZEY}, (t_cplx){e->x, e->y}); ///
			v = get_rat(v);
//			c = appratio((t_cplx){e->x1, e->y1}, (t_cplx){e->x2, e->y2}, r);  ////
			v = app_rat(*e, v);
			z.r = 0;
			z.i = 0;
			e->i = 0;

			while (((z.r * z.r) + (z.i * z.i)) < 4 && (e->i < e->imax))
			{
				float tmp = z.r;
//				z.r = (z.r * z.r) - (z.i * z.i) + c.r;      ///
				z.r = (z.r * z.r) - (z.i * z.i) + v.r;
				z.i = (2 * z.i * tmp) + v.i;
				e->i++;
//				env->tabimg[x + (y * SIZEX)] = i * 3 + (i * 4 << 8) + (i * 5 >> 16);
				e->timg[e->x + (e->y * SIZEX)] = (e->i * 10) + (e->i * 10 << 8) + (e->i * 10 >> 16);
//				e->timg[x + (y * SIZEX)] = ((e->i * 10) % 255) + ((e->i * 10) % 255 << 8) + ((e->i * 10) % 255 >> 16);
			}
//			if (e->i == e->imax)
//				e->timg[e->x + (e->y * SIZEX)] = 0x000000;
			e->y++;
		}
		e->y = 0;
		e->x++;
	}
	mlx_put_image_to_window(e->m->mlx, e->m->win, e->m->img, 0, 0);
}
