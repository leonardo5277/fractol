/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lmunoz-q <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/25 21:09:20 by lmunoz-q          #+#    #+#             */
/*   Updated: 2018/04/10 17:26:50 by lmunoz-q         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../minilibx_macos/mlx.h"
#include "../inc/fractol.h"
#include "../libft/libft.h"
#include <stdio.h>
#include <stdlib.h>

static void	usage(char *av)
{
	ft_putstr("usage: ");
	ft_putstr(av);
	ft_putstr(" [mandelbrot]\n");
}

int main(int ac, char **av)
{
	t_e		e;
	t_m		m;

	if (ac == 2 && name(av[1]))
	{
		ft_bzero(&e, sizeof(e));
		e.m = &m;
		e.m->mlx = mlx_init();
		e.m->win = mlx_new_window(e.m->mlx, SIZEX, SIZEY, "envol");
		e.m->img = mlx_new_image(e.m->mlx, SIZEX, SIZEY);
		e.timg = (int *)mlx_get_data_addr(e.m->img, &(e.m->bbp), &(e.m->size), &(e.m->endn));
		fractale(&e, av[1]);
		mlx_key_hook(e.m->win, key_hook, &e);
		mlx_mouse_hook(e.m->win, mouse_hook, &e);
		mlx_loop(e.m->mlx);
	}
	else
		usage(av[0]);
	return (0);
}
