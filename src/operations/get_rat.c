/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_rat.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lmunoz-q <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/10 17:38:33 by lmunoz-q          #+#    #+#             */
/*   Updated: 2018/04/10 22:36:28 by lmunoz-q         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/fractol.h"

t_v		get_rat(t_v v)
{
	float ratxa;
	float ratya;
	float ratxb;
	float ratyb;

	ratxa = v.drx - v.ulx;
	ratya = v.dry - v.uly;

	ratxb = v.ptx - v.ulx;
	ratyb = v.pty - v.uly;

	v.r = ratxb / ratxa;
	v.i = ratyb / ratya;
	return (v);
}
